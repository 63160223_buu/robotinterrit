/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.worawit.robotproject;

import java.util.Scanner;

/**
 *
 * @author werapan
 */
public class MainProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(10, 10);
        Robot robot = new Robot(0, 0, 'x', map, 20);
        Bomb bomb = new Bomb(5, 5);
                map.addObj(new Tree(7, 1));
        map.addObj(new Tree(2, 2));
        map.addObj(new Tree(8, 8));
        map.addObj(new Tree(2, 8)); //Priority =low= Add before =high=
        map.addObj(new Tree(7, 4));
        map.addObj(new Tree(3, 4));
        map.addObj(new Tree(6, 4));
        map.addObj(new Tree(4, 4));
        map.addObj(new Tree(5, 4));
        map.addObj(new Tree(7, 5));
        map.addObj(new Tree(3, 5));
        map.addObj(new Fuel(8, 5, 5));
        map.addObj(new Fuel(3, 2, 8));
        map.addObj(new Fuel(5, 9, 3));
        map.setBomb(bomb);
        map.setRobot(robot); //Priority =High= Add after =low=

        while (true) {
            map.showMap();
            // W,a| N,w| E,d| S, s| q: quit
            char direction = inputDirection(sc);
            if (direction == 'q') {
                printByeBye();
                break;
            }
            robot.walk(direction);
        }

    }

    private static void printByeBye() {
        System.out.println("Bye Bye!!!");
    }

    private static char inputDirection(Scanner sc) {
        String str = sc.next();
        return str.charAt(0);
    }
}
